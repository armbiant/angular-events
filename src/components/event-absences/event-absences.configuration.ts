export const EVENT_ABSENCES_CONFIGURATION = {
  "title": "StudentPeriodRegistrations.Courses",
  "model": "StudentCourseClasses",
  "searchExpression": "((indexof(student/person/familyName, '${text}') ge 0 or indexof(student/person/givenName, '${text}') ge 0 or indexof(student/studentIdentifier, '${text}') ge 0) and (student/studentStatus/alternateName eq 'active'))",
  "selectable": false,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "hidden": true
    },

    {
      "name": "student/familyName",
      "property": "familyName",
      "title": "Last Name"
    },
    {
      "name": "student/givenName",
      "property": "givenName",
      "title": "Student Name"
    },
    {
      "name": "student/fatherName",
      "property": "fatherName",
      "title": "Father Name"
    },
    {
      "name": "student/studentIdentifier",
      "property": "studentIdentifier",
      "title": "Student Identifier"
    },
    {
      "name": "student/registrationSemester",
      "property": "registrationSemester",
      "title": "Semester"
    },
    {
      "name": "studentCourseClass",
      "property": "studentCourseClass",
      "title": "studentCourseClass",
      "hidden": true
    },
    {
      "name": "attendances",
      "property": "attendanceRecords",
      "virtual": true,
      "sortable": false,
      "title": "NumberOfAbsences",
      "className": "text-center",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${attendances && Array.isArray(attendances) ?'<span class=\"text-center\">' + attendances.filter(x=> x.additionalType === 'TeachingEventAbsence').reduce((a,b) => a + b.coefficient ,0) + '</span>':'-'}"
        }
      ]
    },
    {
      "name": "attendances",
      // "title":"Attendances",
      "formatter": "AbsenceFormatter",
      "hidden": false,
      "sortable": false
    }
  ],
  "defaults": {
    "filter": "(student/studentStatus/alternateName eq 'active')"
  },
  "criteria": [
    {
      "name": "studentGivenName",
      "filter": "(indexof(student/person/givenName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentFamilyName",
      "filter": "(indexof(student/person/familyName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentIdentifier",
      "filter": "(indexof(student/uniqueIdentifier, '${value}') ge 0 or indexof(student/studentIdentifier, '${value}') ge 0 or indexof(student/studentInstituteIdentifier, '${value}') ge 0)",
      "type": "text"
    }
  ]
};
