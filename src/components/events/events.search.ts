export const EVENT_SEARCH_CONFIGURATION = {
  "model": "",
  "components": [
    {
      "label": "Columns",
      "columns": [
        {
          "components": [
            {
              "label": "Events.Name",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "eventName",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Events.Sections",
              "widget": "choicesjs",
              "tableView": true,
              "template": "{{ item.name }}",
              "dataSrc": "url",
              "selectValues": "value",
              "valueProperty": "section",
              "data": {
                "url": "{{data.input.endpoint}}",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "required": false
              },
              "key": "sections",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
      ],
      "tableView": false,
      "key": "department",
      "type": "columns",
      "input": false,
      "path": "columns"
    },
    {
      "label": "Columns",
      "columns": [
        {
          "components": [
            {
              "label": "Events.EventsAfter",
              "tableView": false,
              "enableTime": false,
              "enableMinDateInput": false,
              "datePicker": {
                "disableWeekends": false,
                "disableWeekdays": false
              },
              "enableMaxDateInput": false,
              "key": "minDate",
              "type": "datetime",
              "input": true,
              "widget": {
                "type": "calendar",
                "displayInTimezone": "viewer",
                "locale": "en",
                "useLocaleSettings": false,
                "allowInput": true,
                "mode": "single",
                "enableTime": true,
                "noCalendar": false,
                "format": "yyyy-MM-dd",
                "hourIncrement": 1,
                "minuteIncrement": 1,
                "time_24hr": true,
                "disableWeekends": false,
                "disableWeekdays": false
              }
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Events.EventsBefore",
              "tableView": false,
              "enableMinDateInput": false,
              "enableTime": false,
              "datePicker": {
                "disableWeekends": false,
                "disableWeekdays": false
              },
              "enableMaxDateInput": false,
              "key": "maxDate",
              "type": "datetime",
              "input": true,
              "widget": {
                "type": "calendar",
                "displayInTimezone": "viewer",
                "useLocaleSettings": true,
                "allowInput": true,
                "mode": "single",
                "enableTime": true,
                "noCalendar": false,
                "time_24hr": false,
                "disableWeekends": false,
                "disableWeekdays": false
              }
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Events.Status",
              "widget": "choicesjs",
              "tableView": true,
              "template": "{{ item.name }}",
              "dataSrc": "url",
              "selectValues": "value",
              "valueProperty": "id",
              "data": {
                "url": "/EventStatusTypes?$select=id, name",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "required": false
              },
              "key": "status",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        }
      ],
      "tableView": false,
      "key": "columns1",
      "type": "columns",
      "input": false,
      "path": "columns1"
    }
  ]
}
