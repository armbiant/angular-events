/* tslint:disable quotemark */
/* tslint:disable max-line-length */
export const en = {
    "Date": "Date",
    "Time": "Time",
    "Name": "Event Name",
    "Last Name": "Last Name",
    "Student Name": "Student Name",
    "Father Name": "Father Name",
    "Student Identifier": "Student Identifier",
    "Semester": "Semester",
    "Attendances": "Attendances",
    "Description": "Description",
    "Status": "Status",
    "NumberOfAbsences": "Absences",
    "Performer": "Organizer",
    "Sections": "Sections",
    "Events": {
      "NoUpcomingEventsFound": "No upcoming events were found.",
      "UpcomingEvents": "Upcoming Events",
      "show": "Show",
      "newSimpleEvent": "New event",
      "newRecursiveEvent": "New recursive event",
      "AllPresent": "All present",
      "AllPresentTooltip": "With this action, you are going to mark all students as present disregarding any previous selection.",
      "Save": "Save",
      "SaveTooltip": "With this action, you are going to save the absence records you set. It's recommended to use this action, when you want to come back to this presence list and set more absence records.",
      "Commit": "Submit",
      "CommitTooltip": "With his action, you are going to save and finalize all the set absence records. You will not be able to edit them and they will appear to the students.",
      "Back": "Back",
      "CloseEvent": "Close Event",
      "OpenEvent": "Open Event",
      "ActionCompleted": "Action Completed",
      "MarkedCompleted": "Marked current event as completed",
      "AllSavedPresent": "Marked and saved all students as present",
      "CreatedNew": "New Event",
      "CreatedSuccess": "New event successfully created",
      "Warning": "Warning",
      "AllPresentCheck": "This action saves all students in this event as present. Are you sure you want to proceed?",
      "SavedPresent": "Present students saved",
      "SavedAbsent": "Absent students saved",
      "Absent": "Mark as absent",
      "Present": "Mark as present",
      "CommitCheck": "This action cannot be reversed. Are you sure you want to proceed?",
      "Month": "Month",
      "Week": "Week",
      "Day": "Day",
      "This": {
        "Day": "Today",
        "Month": "This month",
        "Week": "This week"
      },
      "Next": {
        "Day": "Tomorrow",
        "Month": "Next month",
        "Week": "Next week"
      },
      "Previous": {
        "Day": "Yesterday",
        "Month": "Previous month",
        "Week": "Previous week"
      },
      "Export": "Export",
      "EventCompleted": "Completed event",
      "EventOpened" : "Open event",
      "OpenAction": {
        "Title": "Open teaching event",
        "Description": "This action is going to change the status of the selected teaching events to open. An open teaching event can be modified by the instructors and it will be visible both in the students' and the instructors' calendar."
      },
      "CloseAction": {
        "Title": "Close teaching event",
        "Description": "This action is going to change the status of the selected teaching events to completed. A completed teaching event cannot be modified by the instructors and they won't be able to add attendance records for that event."
      },
      "Student": {
        "FamilyName": "Family Name",
        "GivenName": "Given Name",
        "Identifier": "Student Identifier"
      },
      "DeleteEvent": "Deletion action cannot be reversed. Are you sure you want to proceed?",
      "MarkedDeleted": "The event was successfully deleted.",
      "PresenceList": "Presence List",
      "Events": "Teaching Events",
      "Name": "Event name",
      "Sections": "Class section",
      "EventsBefore": "Start date before",
      "EventsAfter": "Start date after",
      "Status": "Event status"
    },

};
/* tslint:enable max-line-length */
/* tslint:enable quotemark */
